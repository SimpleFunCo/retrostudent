package oliinyk.danylo.retrostudents.helper;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.nio.ByteBuffer;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import oliinyk.danylo.retrostudents.model.CoursePOJO;

public class Utils {

    public static boolean listViewReady = false;

    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();

        return (networkInfo != null && networkInfo.isConnectedOrConnecting());
    }

    public static String listToString(List<CoursePOJO> listToConvert) {
//        Timber.d("listToString: " + new Gson().toJson(listToConvert));
        Gson gson = new Gson();
//        return gson.toJson(listToConvert).replaceAll("\"", "/!");
        return gson.toJson(listToConvert);
    }

    public static List<CoursePOJO> stringToList(String stringToConvert) {
//        Timber.d("stringToList: " + stringToConvert);
        Type type = new TypeToken<List<CoursePOJO>>() {}.getType();
        Gson gson = new Gson();
//        stringToConvert.replaceAll("/!", "\"");
        ArrayList<CoursePOJO> coursePOJOs = gson.fromJson(stringToConvert, type);

        return coursePOJOs;
    }

    public static UUID asUuid(byte[] bytes) {
        ByteBuffer bb = ByteBuffer.wrap(bytes);
        long firstLong = bb.getLong();
        long secondLong = bb.getLong();
        return new UUID(firstLong, secondLong);
    }

    public static byte[] asBytes(UUID uuid) {
        ByteBuffer bb = ByteBuffer.wrap(new byte[16]);
        bb.putLong(uuid.getMostSignificantBits());
        bb.putLong(uuid.getLeastSignificantBits());
        return bb.array();
    }

    public static String millisToHumanDate(String millis) {
        Date date = new Date(Long.valueOf(millis));
        DateFormat formatter = new SimpleDateFormat("dd.MM.yyyy");
//        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd kk:mm:ss.SSS");

        return formatter.format(date);
    }
}
