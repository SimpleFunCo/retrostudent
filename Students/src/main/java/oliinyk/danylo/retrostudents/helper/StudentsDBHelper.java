package oliinyk.danylo.retrostudents.helper;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteStatement;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import oliinyk.danylo.retrostudents.model.CoursePOJO;
import oliinyk.danylo.retrostudents.model.StudentPOJO;
import timber.log.Timber;

import static oliinyk.danylo.retrostudents.helper.Constants.DATABASE.AVG_MARK;
import static oliinyk.danylo.retrostudents.helper.Constants.DATABASE.BIRTHDAY;
import static oliinyk.danylo.retrostudents.helper.Constants.DATABASE.CHECK_IF_EMPTY;
import static oliinyk.danylo.retrostudents.helper.Constants.DATABASE.COURSE_NAME;
import static oliinyk.danylo.retrostudents.helper.Constants.DATABASE.CREATE_TABLE_COURSE;
import static oliinyk.danylo.retrostudents.helper.Constants.DATABASE.CREATE_TABLE_STUDENT;
import static oliinyk.danylo.retrostudents.helper.Constants.DATABASE.CREATE_TABLE_STUDENT_COURSE;
import static oliinyk.danylo.retrostudents.helper.Constants.DATABASE.DB_NAME;
import static oliinyk.danylo.retrostudents.helper.Constants.DATABASE.DB_VERSION;
import static oliinyk.danylo.retrostudents.helper.Constants.DATABASE.DROP_COURSE;
import static oliinyk.danylo.retrostudents.helper.Constants.DATABASE.DROP_STUDENT;
import static oliinyk.danylo.retrostudents.helper.Constants.DATABASE.DROP_STUDENT_COURSE;
import static oliinyk.danylo.retrostudents.helper.Constants.DATABASE.FIRST_NAME;
import static oliinyk.danylo.retrostudents.helper.Constants.DATABASE.GET_ALL_STUDENTS;
import static oliinyk.danylo.retrostudents.helper.Constants.DATABASE.GET_AVG_MARK_END;
import static oliinyk.danylo.retrostudents.helper.Constants.DATABASE.GET_AVG_MARK_START;
import static oliinyk.danylo.retrostudents.helper.Constants.DATABASE.GET_COURSE_MARK_END;
import static oliinyk.danylo.retrostudents.helper.Constants.DATABASE.GET_COURSE_MARK_START;
import static oliinyk.danylo.retrostudents.helper.Constants.DATABASE.ID_STUDENT;
import static oliinyk.danylo.retrostudents.helper.Constants.DATABASE.INSERT_COURSE;
import static oliinyk.danylo.retrostudents.helper.Constants.DATABASE.INSERT_STUDENT;
import static oliinyk.danylo.retrostudents.helper.Constants.DATABASE.INSERT_STUDENT_COURSE;
import static oliinyk.danylo.retrostudents.helper.Constants.DATABASE.LAST_NAME;
import static oliinyk.danylo.retrostudents.helper.Constants.DATABASE.MARK;
import static oliinyk.danylo.retrostudents.helper.Constants.DATABASE.STUDENTS_BY_COURSE_END;
import static oliinyk.danylo.retrostudents.helper.Constants.DATABASE.STUDENTS_BY_COURSE_MARK_END;
import static oliinyk.danylo.retrostudents.helper.Constants.DATABASE.STUDENTS_BY_COURSE_MARK_START;
import static oliinyk.danylo.retrostudents.helper.Constants.DATABASE.STUDENTS_BY_COURSE_START;
import static oliinyk.danylo.retrostudents.helper.Constants.DATABASE.STUDENTS_BY_MARK;
import static oliinyk.danylo.retrostudents.helper.Utils.listViewReady;

public class StudentsDBHelper extends SQLiteOpenHelper {

    private boolean fulfilled = false;

    public StudentsDBHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
        Timber.d("Constructing DB");
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        Timber.d("Creating DB");
        try {
            db.execSQL(CREATE_TABLE_STUDENT);
            db.execSQL(CREATE_TABLE_COURSE);
            db.execSQL(CREATE_TABLE_STUDENT_COURSE);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(DROP_STUDENT);
        db.execSQL(DROP_COURSE);
        db.execSQL(DROP_STUDENT_COURSE);
        this.onCreate(db);
    }

    public synchronized void fillDatabase(List<StudentPOJO> students) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();
        SQLiteStatement statement = db.compileStatement(INSERT_STUDENT);
        Map<String, Long> uniqueCourses = new HashMap<>();
        Long id_course;
        byte startToFill = 0;
        for (StudentPOJO student : students) {
            statement.bindString(1, student.getId());
            statement.bindString(2, student.getFirstName());
            statement.bindString(3, student.getLastName());
            statement.bindLong(4, Long.parseLong(student.getBirthday()));

            Long id_student = statement.executeInsert();
            statement.clearBindings();
            for (CoursePOJO course : student.getCoursePOJOs()) {
                if (!uniqueCourses.containsKey(course.getName())) {
                    id_course = addCourse(db, course.getName());
                    uniqueCourses.put(course.getName(), id_course);
                } else {
                    id_course = uniqueCourses.get(course.getName());
                }
                addMark(db, id_student, id_course, course.getMark());
            }

            if (startToFill < 20) {
                startToFill++;
            } else if (startToFill >= 19 && !listViewReady) {
                listViewReady = true;
            }
        }
        fulfilled = true;

        db.setTransactionSuccessful();
        db.endTransaction();
        db.close();
    }


    public Long addCourse(SQLiteDatabase db,String course) {
        SQLiteStatement statement = db.compileStatement(INSERT_COURSE);

        statement.bindString(1, course);
        Long id_course = statement.executeInsert();
        statement.clearBindings();

        return id_course;
    }

    public void addMark(SQLiteDatabase db,Long id_student, Long id_course, Double id_mark) {
        SQLiteStatement statement = db.compileStatement(INSERT_STUDENT_COURSE);

        statement.bindLong(1, id_student);
        statement.bindLong(2, id_course);
        statement.bindDouble(3, id_mark);

        statement.executeInsert();
        statement.clearBindings();
    }

    public List<StudentPOJO> getByCourse(String course) {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(
                STUDENTS_BY_COURSE_START +
                        course + STUDENTS_BY_COURSE_END, null);
        List<StudentPOJO> students = new ArrayList<>();

        if (cursor.getCount() > 0) {
            if (cursor.moveToFirst()) {
                do {
                    StudentPOJO student = new StudentPOJO();
                    student.setStudentId(cursor.getLong(
                            cursor.getColumnIndex(ID_STUDENT)));
                    student.setFirstName(cursor.getString(
                            cursor.getColumnIndex(FIRST_NAME)));
                    student.setLastName(cursor.getString(
                            cursor.getColumnIndex(LAST_NAME)));
                    student.setBirthday(cursor.getString(
                            cursor.getColumnIndex(BIRTHDAY)));
                    students.add(student);
                } while (cursor.moveToNext());
            }
        }

        db.close();
        return students;
    }

    public List<StudentPOJO> getByMark(Double mark) {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(STUDENTS_BY_MARK + mark, null);
        List<StudentPOJO> students = new ArrayList<>();

        if (cursor.getCount() > 0) {
            if (cursor.moveToFirst()) {
                do {
                    StudentPOJO student = new StudentPOJO();
                    student.setStudentId(cursor.getLong(
                            cursor.getColumnIndex(ID_STUDENT)));
                    student.setFirstName(cursor.getString(
                            cursor.getColumnIndex(FIRST_NAME)));
                    student.setLastName(cursor.getString(
                            cursor.getColumnIndex(LAST_NAME)));
                    student.setBirthday(cursor.getString(
                            cursor.getColumnIndex(BIRTHDAY)));
                    students.add(student);
                } while (cursor.moveToNext());
            }
        }

        db.close();
        return students;
    }

    public List<StudentPOJO> getByCourseMark(String course, Double mark) {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(
                STUDENTS_BY_COURSE_MARK_START +
                        course + STUDENTS_BY_COURSE_MARK_END + mark, null);
        List<StudentPOJO> students = new ArrayList<>();

        if (cursor.getCount() > 0) {
            if (cursor.moveToFirst()) {
                do {
                    StudentPOJO student = new StudentPOJO();
                    student.setStudentId(cursor.getLong(
                            cursor.getColumnIndex(ID_STUDENT)));
                    student.setFirstName(cursor.getString(
                            cursor.getColumnIndex(FIRST_NAME)));
                    student.setLastName(cursor.getString(
                            cursor.getColumnIndex(LAST_NAME)));
                    student.setBirthday(cursor.getString(
                            cursor.getColumnIndex(BIRTHDAY)));
                    students.add(student);
                } while (cursor.moveToNext());
            }
        }

        db.close();
        return students;
    }

    public boolean isDbFilled() {
        return fulfilled;
    }

    public List<StudentPOJO> getStudents() {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(GET_ALL_STUDENTS, null);
        List<StudentPOJO> students = new ArrayList<>();

        if (cursor.getCount() > 0) {
            if (cursor.moveToFirst()) {
                do {
                    StudentPOJO student = new StudentPOJO();
                    student.setStudentId(cursor.getLong(
                            cursor.getColumnIndex(ID_STUDENT)));
                    student.setFirstName(cursor.getString(
                            cursor.getColumnIndex(FIRST_NAME)));
                    student.setLastName(cursor.getString(
                            cursor.getColumnIndex(LAST_NAME)));
                    student.setBirthday(cursor.getString(
                            cursor.getColumnIndex(BIRTHDAY)));
                    students.add(student);
                } while (cursor.moveToNext());
            }
        }

        db.close();
        return students;
    }

    public List<CoursePOJO> getCoursesAndMarks(Long studentId) {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(GET_COURSE_MARK_START +
                studentId + GET_COURSE_MARK_END, null);
        List<CoursePOJO> courses = new ArrayList<>();

        if (cursor.getCount() > 0) {
            if (cursor.moveToFirst()) {
                do {
                    CoursePOJO course = new CoursePOJO();
                    course.setName(cursor.getString(
                            cursor.getColumnIndex(COURSE_NAME)));
                    course.setMark(cursor.getDouble(
                            cursor.getColumnIndex(MARK)));
                    courses.add(course);
                } while (cursor.moveToNext());
            }
        }

        db.close();
        return courses;
    }

    public Double getAverageMark(Long studentId) {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(GET_AVG_MARK_START +
                studentId + GET_AVG_MARK_END, null);
        Double averageMark = 0D;

        if (cursor.getCount() > 0) {
            if (cursor.moveToFirst()) {
                do {
                    averageMark = cursor.getDouble(
                            cursor.getColumnIndex(AVG_MARK));
                } while (cursor.moveToNext());
            }
        }

        db.close();
        return averageMark;
    }

    public synchronized boolean isDBEmpty() {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(CHECK_IF_EMPTY, null);
        cursor.moveToFirst();

        if (cursor.getInt(0) == 0) {
            db.close();
            return true;
        } else {
            db.close();
            return false;
        }
    }
}
