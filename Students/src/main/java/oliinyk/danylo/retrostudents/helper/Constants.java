package oliinyk.danylo.retrostudents.helper;

public class Constants {

    public static final class HTTP {

        public static final String STUDENTS_URL = "https://ddapp-sfa-api-dev.azurewebsites.net/api/test/";
    }

    public static final class DATABASE {

        public static final String DB_NAME = "students";
        public static final int DB_VERSION = 1;
        public static final String TABLE_STUDENT = "student";
        public static final String TABLE_COURSE = "course";
        public static final String TABLE_STUDENT_COURSE = "student_course";

        public static final String ID_STUDENT = "_id_student";
        public static final String UUID_STUDENT = "uuid_student";
        public static final String ID_COURSE = "_id_course";
        public static final String ID_STUDENT_COURSE = "_id_student_course";
//        public static final String UUID_STUDENT = "_id";
        public static final String FIRST_NAME = "first_name";
        public static final String LAST_NAME = "last_name";
        public static final String BIRTHDAY = "birthday";
        public static final String COURSE_NAME = "course_name";
        public static final String MARK = "mark";
        public static final String AVG_MARK = "avg_mark";

        public static final String GET_ALL_STUDENTS = "SELECT * FROM " + TABLE_STUDENT;
        public static final String STUDENTS_BY_COURSE_START =
                "SELECT ST." + ID_STUDENT + ", " + FIRST_NAME + ", " + LAST_NAME + ", " + BIRTHDAY +
                        " FROM " + TABLE_STUDENT + " as ST, " + TABLE_STUDENT_COURSE + " as SC, " +
                        TABLE_COURSE + " as CO WHERE " + COURSE_NAME + " = \"";
        public static final String STUDENTS_BY_COURSE_END =
                "\" AND CO." + ID_COURSE + " = SC." + ID_COURSE + " AND ST." + ID_STUDENT +" = SC." + ID_STUDENT;
        public static final String STUDENTS_BY_MARK =
                "SELECT ST." + ID_STUDENT +", " + FIRST_NAME + ", " + LAST_NAME + ", " + BIRTHDAY + " FROM " + TABLE_STUDENT +
                        " as ST, " + TABLE_STUDENT_COURSE + " as SC, " +
                        TABLE_COURSE + " as CO WHERE CO." + ID_COURSE + " = SC." +
                        ID_COURSE + " AND ST." + ID_STUDENT +
                        " = SC." + ID_STUDENT + " AND " + MARK + " = ";
        public static final String STUDENTS_BY_COURSE_MARK_START =
                "SELECT ST." + ID_STUDENT + ", " + FIRST_NAME + ", " + LAST_NAME + ", " + BIRTHDAY +
                        " FROM " + TABLE_STUDENT + " as ST, " + TABLE_STUDENT_COURSE + " as SC, " +
                        TABLE_COURSE + " as CO WHERE " + COURSE_NAME + " = \"";
        public static final String STUDENTS_BY_COURSE_MARK_END =
                "\" AND CO." + ID_COURSE + " = SC." + ID_COURSE + " AND ST." + ID_STUDENT +
                        " = SC." + ID_STUDENT + " AND " + MARK + " = ";
                public static final String DROP_STUDENT = "DROP TABLE IF EXISTS" + TABLE_STUDENT;
                public static final String DROP_COURSE = "DROP TABLE IF EXISTS" + TABLE_COURSE;
                public static final String DROP_STUDENT_COURSE = "DROP TABLE IF EXISTS" + TABLE_STUDENT_COURSE;
        public static final String INSERT_STUDENT = "INSERT INTO " + TABLE_STUDENT +
                " (" + UUID_STUDENT + ", " +
                FIRST_NAME + ", " +
                LAST_NAME + ", " +
                BIRTHDAY + ")" +
                " VALUES (?, ?, ?, ?);";
        public static final String INSERT_COURSE = "INSERT INTO " + TABLE_COURSE +
                " (" + COURSE_NAME + ")" +
                " VALUES (?);";
        public static final String INSERT_STUDENT_COURSE = "INSERT INTO " + TABLE_STUDENT_COURSE +
                " (" + ID_STUDENT + ", " +
                ID_COURSE + ", " +
                MARK + ")" +
                " VALUES (?, ?, ?);";
        public static final String CREATE_TABLE_STUDENT = "CREATE TABLE " + TABLE_STUDENT +
                " (" + ID_STUDENT + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, " +
                UUID_STUDENT + " TEXT, " +
                FIRST_NAME + " TEXT, " +
                LAST_NAME + " TEXT, " +
                BIRTHDAY + " INTEGER" + ")";
        public static final String CREATE_TABLE_COURSE = "CREATE TABLE " + TABLE_COURSE +
                " (" + ID_COURSE + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, " +
                COURSE_NAME + " TEXT" + ")";
        public static final String CREATE_TABLE_STUDENT_COURSE = "CREATE TABLE " + TABLE_STUDENT_COURSE +
                " (" + ID_STUDENT_COURSE + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, " +
                ID_STUDENT + " INTEGER, " +
                ID_COURSE + " INTEGER, " +
                MARK + " REAL, " +
                "FOREIGN KEY(" + ID_STUDENT + ")" +
                " REFERENCES " + TABLE_STUDENT + "(" + ID_STUDENT + "), " +
                "FOREIGN KEY(" + ID_COURSE + ")" +
                " REFERENCES " + TABLE_COURSE + "(" + ID_COURSE + ")" +
                ")";
        public static final String CHECK_IF_EMPTY = "SELECT count(*) FROM " + TABLE_STUDENT;
        public static final String GET_COURSE_MARK_START = "SELECT " + COURSE_NAME + ", " + MARK +
                " FROM " + TABLE_STUDENT + " as ST, " +
                TABLE_STUDENT_COURSE + " as SC, " + TABLE_COURSE + " as CO WHERE ST." + ID_STUDENT + " = ";
        public static final String GET_COURSE_MARK_END = " AND CO." + ID_COURSE + "=SC." + ID_COURSE + " " +
                " AND ST." + ID_STUDENT + "=SC." + ID_STUDENT;
        public static final String GET_AVG_MARK_START = "SELECT AVG(" + MARK + ") as " +
                AVG_MARK + " FROM " + TABLE_STUDENT + " as ST, " +
                TABLE_STUDENT_COURSE + " as SC, " + TABLE_COURSE + " as CO WHERE ST." + ID_STUDENT + " = ";
        public static final String GET_AVG_MARK_END = " AND CO." + ID_COURSE + "=SC." + ID_STUDENT +
                " AND ST." + ID_STUDENT + "=SC." + ID_STUDENT;
    }
}
