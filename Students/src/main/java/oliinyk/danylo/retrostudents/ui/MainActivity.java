package oliinyk.danylo.retrostudents.ui;

import android.app.Activity;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.stetho.Stetho;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.Observable;
import io.reactivex.Scheduler;
import io.reactivex.schedulers.Schedulers;
import oliinyk.danylo.retrostudents.BuildConfig;
import oliinyk.danylo.retrostudents.R;
import oliinyk.danylo.retrostudents.adapter.StudentAdapter;
import oliinyk.danylo.retrostudents.controller.EndlessScrollListener;
import oliinyk.danylo.retrostudents.controller.RestManager;
import oliinyk.danylo.retrostudents.helper.StudentsDBHelper;
import oliinyk.danylo.retrostudents.helper.Utils;
import oliinyk.danylo.retrostudents.model.CoursePOJO;
import oliinyk.danylo.retrostudents.model.StudentPOJO;
import timber.log.Timber;

import static oliinyk.danylo.retrostudents.helper.Utils.listViewReady;

public class MainActivity extends Activity implements StudentAdapter.OnRecyclerItemClickListener {

    private static final String TAG = "MainLog";
    private static boolean isPlanted = false;

    private final Object lock = new Object();

    @BindView(R.id.rvStudents)
    RecyclerView rvStudents;
    @BindView(R.id.drawerLayout)
    DrawerLayout drawerLayout;
    @BindView(R.id.ibFilter)
    ImageButton ibFilter;
    @BindView(R.id.nvMenu)
    NavigationView nvMenu;
    @BindView(R.id.tvListSize)
    TextView tvListSize;
    @BindView(R.id.tvFetchDatabase)
    TextView tvFetchDatabase;
    @BindView(R.id.pbLoadingItems)
    ProgressBar pbLoadingItems;
    private Spinner ndSpinnerCourses;
    private Spinner ndSpinnerMark;

    private RestManager mManager;
    private List<StudentPOJO> studentsList;
    private List<StudentPOJO> students;
    private StudentAdapter studentAdapter;
    private EndlessScrollListener endlessScroll;
    private StudentsDBHelper studentsDBHelper;
    private boolean scrollIsClear = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_navigation);

        ButterKnife.bind(this);

        if (BuildConfig.DEBUG) {
            if (!isPlanted) { // For hot-swap changes apply
                Timber.plant(new Timber.DebugTree());
                isPlanted = true;
            }

            Stetho.InitializerBuilder initializerBuilder = Stetho.newInitializerBuilder(this);
            initializerBuilder.enableWebKitInspector(Stetho.defaultInspectorModulesProvider(this));
            initializerBuilder.enableDumpapp(Stetho.defaultDumperPluginsProvider(getApplicationContext()));
            Stetho.Initializer initializer = initializerBuilder.build();
            Stetho.initialize(initializer);

//            getApplicationContext().deleteDatabase(Constants.DATABASE.DB_NAME);         //For TEST
        }

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        studentsList = new ArrayList<>();
        students = new ArrayList<>();
        mManager = new RestManager();
        studentAdapter = new StudentAdapter(this);
        studentsDBHelper = new StudentsDBHelper(getApplicationContext());
        Observable<List<StudentPOJO>> fetchStudents = Observable.just(getJSONFromAssets());   // Local JSON For Test
//        Observable<List<StudentPOJO>> fetchStudents = mManager.getStudentApi().getStudents();
        Observable<List<StudentPOJO>> studentsToDatabase = Observable.just(students);
        Scheduler scheduler = Schedulers.from(Executors.newSingleThreadExecutor());

        pbLoadingItems.setVisibility(View.VISIBLE);
        rvStudents.setLayoutManager(layoutManager);
        endlessScroll = new EndlessScrollListener((LinearLayoutManager) layoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                if (totalItemsCount + 20 <= students.size()) {
                    studentAdapter.setStudents(students.subList(0, totalItemsCount + 20));
                } else {
                    studentAdapter.setStudents(students.subList(0, totalItemsCount +
                            (students.size() - totalItemsCount)));
                }
                view.post(() -> studentAdapter.notifyItemInserted(totalItemsCount));

                tvListSize.setText(String.valueOf(studentAdapter.getItemCount()));
            }
        };
        rvStudents.addOnScrollListener(endlessScroll);

        tvFetchDatabase.setOnClickListener(view -> {
            resetEndlessScroll();
            pbLoadingItems.setVisibility(View.VISIBLE);
            if (Utils.isNetworkAvailable(getApplicationContext())) {
                if (studentsDBHelper.isDBEmpty()) {
                    fetchAndFillDB(fetchStudents, studentsToDatabase, scheduler);
                }
            } else {
                pbLoadingItems.setVisibility(View.INVISIBLE);
                Toast.makeText(getApplicationContext(), "Check your connection", Toast.LENGTH_LONG).show();
                return;
            }

            synchronized (lock) {
                while (!scrollIsClear) {
                    try {
                        lock.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                ndSpinnerCourses.setSelection(0, false);
                ndSpinnerMark.setSelection(0, false);

                populateList();
            }
        });

        initDrawer();

        if (Utils.isNetworkAvailable(getApplicationContext())) {
            if (studentsDBHelper.isDBEmpty()) {
                fetchAndFillDB(fetchStudents, studentsToDatabase, scheduler);
            }
        } else {
            Toast.makeText(getApplicationContext(), "Check your connection", Toast.LENGTH_LONG).show();
        }

        populateList();
    }

    private void populateList() {
        Thread listFiller = new Thread(() -> {
            long startTime = System.currentTimeMillis();
            while (!listViewReady || studentsDBHelper.isDBEmpty()) {
                if ((System.currentTimeMillis() - startTime) >= 5000) {
                    runOnUiThread(() -> {
                        pbLoadingItems.setVisibility(View.INVISIBLE);
                        Toast.makeText(getApplicationContext(),
                                "Empty Database. Nothing lo load", Toast.LENGTH_LONG).show();
                    });
                    return;
                } else if (listViewReady) {
                    break;
                } else if (!studentsDBHelper.isDBEmpty()) {
                    break;
                }
            } // Waiting for first 20 elements while fetching from server (5 seconds timeout)

            runOnUiThread(() -> {
                synchronized (lock) {
                    students = studentsDBHelper.getStudents();
                    if (students.size() > 20) {
                        pbLoadingItems.setVisibility(View.INVISIBLE);
                        studentAdapter = new StudentAdapter(students.subList(0, 20), this);
                        tvListSize.setText(String.valueOf(studentAdapter.getItemCount()));
                        rvStudents.setItemAnimator(new DefaultItemAnimator());
                        rvStudents.setAdapter(studentAdapter);
                        studentAdapter.notifyDataSetChanged();
                    } else if (students.size() > 0) {
                        pbLoadingItems.setVisibility(View.INVISIBLE);
                        studentAdapter = new StudentAdapter(students.subList(0, students.size()), this);
                        tvListSize.setText(String.valueOf(studentAdapter.getItemCount()));
                        rvStudents.setItemAnimator(new DefaultItemAnimator());
                        rvStudents.setAdapter(studentAdapter);
                        studentAdapter.notifyDataSetChanged();
                    }

                    while (studentAdapter.getStudents() == null) {
                        try {
                            lock.wait();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }
            });
        });
        listFiller.start();
    }

    private void fetchAndFillDB(Observable<List<StudentPOJO>> fetchStudents,
                                Observable<List<StudentPOJO>> studentsToDatabase,
                                Scheduler scheduler) {
        fetchStudents
                .subscribeOn(scheduler)
                .subscribe(students::addAll);

        studentsToDatabase
                .subscribeOn(scheduler)
                .subscribe(studentsToFill -> {
                    synchronized (lock) {
                        studentsDBHelper.fillDatabase(studentsToFill);
                        while (true) {
                            if (studentsDBHelper.isDbFilled()) {
                                lock.notify();
                                break;
                            }
                            //                        try {
                            //                            lock.wait();
                            //                        } catch (InterruptedException e) {
                            //                            e.printStackTrace();
                            //                        }
                        }
                    }
                });
    }

    private List<StudentPOJO> getJSONFromAssets() {
        Gson gson = new Gson();
        Type type = new TypeToken<List<StudentPOJO>>() {
        }.getType();

        return gson.fromJson(loadJSONFromAsset(), type);
    }

    private void resetEndlessScroll() {
        synchronized (lock) {
            students.clear();
            studentAdapter.setStudents(students);
            studentAdapter.notifyDataSetChanged();
            endlessScroll.resetState();

            scrollIsClear = true;
            try {
                lock.wait(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private void initDrawer() {
        String[] courses = getResources().getStringArray(R.array.courses);
        String[] marks = getResources().getStringArray(R.array.marks);

        ibFilter.setOnClickListener(item -> {
            if (item.getId() == R.id.ibFilter) {
                drawerLayout.openDrawer(GravityCompat.END);
            }
        });

        nvMenu.setNavigationItemSelectedListener(item -> {
            if (item.getItemId() == R.id.ndResetFilters) {
                resetEndlessScroll();
                synchronized (lock) {
                    while (!scrollIsClear) {
                        try {
                            lock.wait();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                    ndSpinnerCourses.setSelection(0, false);
                    ndSpinnerMark.setSelection(0, false);

                    students = studentsDBHelper.getStudents();
                    if (students.size() > 20) {
                        studentAdapter.setStudents(students.subList(0, 20));
                        tvListSize.setText(String.valueOf(studentAdapter.getItemCount()));
                        studentAdapter.notifyDataSetChanged();
                    } else if (students.size() > 0) {
                        studentAdapter.setStudents(students.subList(0, students.size()));
                        tvListSize.setText(String.valueOf(studentAdapter.getItemCount()));
                        studentAdapter.notifyDataSetChanged();
                    }
                }
                return true;
            } else {
                return true;
            }
        });

        ndSpinnerCourses =
                (Spinner) nvMenu.getMenu().findItem(R.id.ndSpinnerCourses).getActionView();
        ArrayAdapter coursesAdapter =
                new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, courses);
        coursesAdapter.setDropDownViewResource(R.layout.spinner_element);
        ndSpinnerCourses.setAdapter(coursesAdapter);
        ndSpinnerCourses.setSelection(0, false); //To avoid initial self-ckecking
        ndSpinnerCourses.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                resetEndlessScroll();
                chooseAndApplyFilter();
                scrollIsClear = false;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        ndSpinnerMark =
                (Spinner) nvMenu.getMenu().findItem(R.id.ndSpinnerMark).getActionView();
        ArrayAdapter marksAdapter =
                new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, marks);
        marksAdapter.setDropDownViewResource(R.layout.spinner_element);
        ndSpinnerMark.setAdapter(marksAdapter);
        ndSpinnerMark.setSelection(0, false); //To avoid initial self-ckecking
        ndSpinnerMark.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                resetEndlessScroll();
                chooseAndApplyFilter();
                scrollIsClear = false;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    private void loadNextItems(int offset) {
        students.subList(offset, offset + 20);
    }

/*    private void testStudent() {
        CoursePOJO coursePOJO1 = new CoursePOJO();
        coursePOJO1.setName("Course-1");
        coursePOJO1.setMark(3.0);
        CoursePOJO coursePOJO2 = new CoursePOJO();
        coursePOJO2.setName("Course-2");
        coursePOJO2.setMark(4.0);
        CoursePOJO coursePOJO3 = new CoursePOJO();
        coursePOJO3.setName("Course-3");
        coursePOJO3.setMark(5.0);

        List<CoursePOJO> coursePOJOs = new ArrayList<>();
        coursePOJOs.add(coursePOJO1);
        coursePOJOs.add(coursePOJO2);
        coursePOJOs.add(coursePOJO3);

        StudentPOJO studentPOJO = new StudentPOJO();
        studentPOJO.setId("14745d61-835a-4d4d-83f4-9920afc13235");
        studentPOJO.setFirstName("Ivan");
        studentPOJO.setLastName("Suharev");
        studentPOJO.setBirthday("22:20:10");
        studentPOJO.setCoursePOJOs(coursePOJOs);

        studentsDBHelper.addStudent(studentPOJO);
    }

    private void jsonTest(List<CoursePOJO> coursePOJOs) {
        Gson gson = new Gson();
        CoursePOJO course1 = new CoursePOJO();
        course1.setName("Course1");
        course1.setMark(3.0);
        CoursePOJO course2 = new CoursePOJO();
        course2.setName("Course2");
        course2.setMark(5.0);

        coursePOJOs = new ArrayList<>();
        coursePOJOs.add(course1);
        coursePOJOs.add(course2);

        String string = gson.toJson(coursePOJOs);

        Timber.d(string);

        Type type = new TypeToken<List<CoursePOJO>>() {}.getType();

        ArrayList<CoursePOJO> coursePOJOs1 = gson.fromJson(string, type);

        Timber.d(coursePOJOs1.get(0).getName());
        Timber.d(coursePOJOs1.get(0).getMark().toString());
    }*/

    public String loadJSONFromAsset() {
        String json;
        try {
            InputStream inputStream = getAssets().open("students.json");
            int size = inputStream.available();
            byte[] buffer = new byte[size];
            inputStream.read(buffer);
            inputStream.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }

        return json;
    }

    private void chooseAndApplyFilter() {
        synchronized (lock) {
            while (!scrollIsClear) {
                try {
                    lock.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            if (ndSpinnerMark.getSelectedItemPosition() == 0 &&
                    ndSpinnerCourses.getSelectedItemPosition() == 0) {

                students = studentsDBHelper.getStudents();
            } else if (ndSpinnerCourses.getSelectedItemPosition() == 0) {
                students = studentsDBHelper.getByMark(
                        Double.parseDouble(ndSpinnerMark.getSelectedItem().toString()));
            } else if (ndSpinnerMark.getSelectedItemPosition() == 0) {
                students = studentsDBHelper.getByCourse(
                        ndSpinnerCourses.getSelectedItem().toString());
            } else {
                students = studentsDBHelper.getByCourseMark(
                        ndSpinnerCourses.getSelectedItem().toString(),
                        Double.parseDouble(ndSpinnerMark.getSelectedItem().toString()));
            }

            if (students.size() > 20) {
                studentAdapter.setStudents(students.subList(0, 20));
                tvListSize.setText(String.valueOf(studentAdapter.getItemCount()));
                studentAdapter.notifyDataSetChanged();
            } else if (students.size() > 0) {
                studentAdapter.setStudents(students.subList(0, students.size()));
                tvListSize.setText(String.valueOf(studentAdapter.getItemCount()));
                studentAdapter.notifyDataSetChanged();
            }
        }
    }

    @Override
    public void onRecyclerItemClick(Long studentId) {
        AlertDialog builder = new AlertDialog.Builder(this).create();
        LayoutInflater layoutInflater = getLayoutInflater();
        List<CoursePOJO> courses = studentsDBHelper.getCoursesAndMarks(studentId);


        View customView = layoutInflater.inflate(R.layout.info_message_layout, null);

        TextView tvMarkOne = (TextView) customView.findViewById(R.id.tvMarkOne);
        TextView tvMarkTwo = (TextView) customView.findViewById(R.id.tvMarkTwo);
        TextView tvMarkThree = (TextView) customView.findViewById(R.id.tvMarkThree);
        TextView tvMarkFour = (TextView) customView.findViewById(R.id.tvMarkFour);
        TextView tvAverageMark = (TextView) customView.findViewById(R.id.tvAverageMark);
        Button btnInfoOk = (Button) customView.findViewById(R.id.btnInfoOk);

        btnInfoOk.setOnClickListener(view -> builder.dismiss());
        tvMarkOne.setText(courses.get(0).getMark().toString());
        tvMarkTwo.setText(courses.get(1).getMark().toString());
        tvMarkThree.setText(courses.get(2).getMark().toString());
        tvMarkFour.setText(courses.get(3).getMark().toString());
        tvAverageMark.setText(studentsDBHelper.getAverageMark(studentId).toString());

        builder.setView(customView);
        builder.create();
        builder.show();
    }
}
