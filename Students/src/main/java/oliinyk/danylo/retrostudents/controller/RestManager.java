package oliinyk.danylo.retrostudents.controller;

import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

import oliinyk.danylo.retrostudents.helper.Constants;
import oliinyk.danylo.retrostudents.model.callback.StudentApi;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RestManager {

    private StudentApi studentApi;

    public StudentApi getStudentApi() {
        if (studentApi == null) {
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(Constants.HTTP.STUDENTS_URL)
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            studentApi = retrofit.create(StudentApi.class);
        }

        return studentApi;
    }
}
