package oliinyk.danylo.retrostudents.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import oliinyk.danylo.retrostudents.R;
import oliinyk.danylo.retrostudents.helper.Utils;
import oliinyk.danylo.retrostudents.model.StudentPOJO;

public class StudentAdapter extends RecyclerView.Adapter<StudentAdapter.Holder> {

    public interface OnRecyclerItemClickListener {
        void onRecyclerItemClick(Long studentId);
    }

    private List<StudentPOJO> students;
    private OnRecyclerItemClickListener onRecyclerItemClickListener;

    public StudentAdapter() {
    }

    public StudentAdapter(OnRecyclerItemClickListener onRecyclerItemClickListener) {
        this.onRecyclerItemClickListener = onRecyclerItemClickListener;
    }

    public StudentAdapter(List<StudentPOJO> students, OnRecyclerItemClickListener onRecyclerItemClickListener) {
        this.students = students;
        this.onRecyclerItemClickListener = onRecyclerItemClickListener;
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View row = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_item, parent, false);

        Holder viewHolder = new Holder(row, onRecyclerItemClickListener);
        return viewHolder;
    }

    @Override
    public int getItemCount() {
        return students.size();
    }

    public List<StudentPOJO> getStudents() {
        return students;
    }

    @Override
    public void onBindViewHolder(final Holder holder, int position) {
        StudentPOJO currentStudent = students.get(position);

        holder.tvFirstName.setText(currentStudent.getFirstName());
        holder.tvLastName.setText(currentStudent.getLastName());
        holder.tvBirthday.setText(Utils.millisToHumanDate(currentStudent.getBirthday()));
        holder.studentId = currentStudent.getStudentId();
    }

    public void addStudent(StudentPOJO student) {
        students.add(student);
        notifyDataSetChanged();
    }

    public void setStudents(List<StudentPOJO> students) {
        this.students = students;
    }

    public StudentPOJO getSelectedStudent(int position) {
        return students.get(position);
    }

    public class Holder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.tvFirstName)
        TextView tvFirstName;
        @BindView(R.id.tvLastName)
        TextView tvLastName;
        @BindView(R.id.tvBirthday)
        TextView tvBirthday;
        @BindView(R.id.ibInfo)
        ImageButton ibInfo;
        private OnRecyclerItemClickListener onRecyclerItemClickListener;
        private Long studentId;


        public Holder(View itemView, OnRecyclerItemClickListener onRecyclerItemClickListener) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            this.onRecyclerItemClickListener = onRecyclerItemClickListener;
            ibInfo.setOnClickListener(this);
        }



        @Override
        public void onClick(View v) {
            onRecyclerItemClickListener.onRecyclerItemClick(studentId);
        }

        public Long getStudentId() {
            return studentId;
        }

        public void setStudentId(Long studentId) {
            this.studentId = studentId;
        }
    }
}
