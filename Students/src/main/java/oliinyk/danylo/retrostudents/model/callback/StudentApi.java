package oliinyk.danylo.retrostudents.model.callback;

import java.util.List;

import io.reactivex.Observable;
import okhttp3.ResponseBody;
import oliinyk.danylo.retrostudents.model.StudentPOJO;
import retrofit2.http.GET;
import retrofit2.http.Streaming;

public interface StudentApi {
    @GET("students/")
    Observable<List<StudentPOJO>> getStudents();

    @Streaming
    @GET("students/")
    Observable<ResponseBody> getStudentsStream();
}
