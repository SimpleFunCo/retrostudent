package oliinyk.danylo.retrostudents.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CoursePOJO {

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("mark")
    @Expose
    private Double mark;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getMark() {
        return mark;
    }

    public void setMark(Double mark) {
        this.mark = mark;
    }
}
