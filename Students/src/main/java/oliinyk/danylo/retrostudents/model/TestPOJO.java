package oliinyk.danylo.retrostudents.model;
/* Created on 2/25/2017. */

import java.util.List;

public class TestPOJO {

    private List<StudentPOJO> studentPOJOs;

    public List<StudentPOJO> getStudentPOJOs() {
        return studentPOJOs;
    }

    public void setStudentPOJOs(List<StudentPOJO> studentPOJOs) {
        this.studentPOJOs = studentPOJOs;
    }
}
